
-----

made it :D

# Fork me to get started with energy & environmental impact measurements 

You can find the full documentation [here.](https://softawere-hackathon.gitlab.io/documentation/tutorials/getting_started.html)

This project uses a Debian Bullseye image for running CI/CD jobs and includes a sample strict that runs a stress test in Python.

You can customize & change everything. 

If you need a different base image, check out the [base image runners](https://gitlab.com/softawere-hackathon/base-runner-container) or install the dependencies yourself during the build process.